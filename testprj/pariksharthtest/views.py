from django.shortcuts import render
from .models import register

# Create your views here.

def index(request):
    if request.method == 'POST':
        if request.POST.get('Name') and request.POST.get('PhoneNo') and request.POST.get('addr'):
            post = register()
            post.Name = request.POST['Name']
            post.PhoneNo = request.POST['PhoneNo']
            post.addr = request.POST['addr']
            post.save()

            return render(request, 'register.html')

    else:
        return render(request, 'register.html')
